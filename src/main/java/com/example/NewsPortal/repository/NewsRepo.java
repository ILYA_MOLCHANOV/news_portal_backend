
/*Этот код определяет интерфейс NewsRepo, который является репозиторием для работы с сущностями
        NewsEntity в Spring приложении.*/

package com.example.NewsPortal.repository;

import org.springframework.data.repository.CrudRepository;

import com.example.NewsPortal.entitys.NewsEntity;

public interface NewsRepo extends CrudRepository<NewsEntity, Long> {
    
}

   /* package com.example.NewsPortal.repository;

   Эта строка определяет пакет, в котором находится интерфейс NewsRepo.

   import org.springframework.data.repository.CrudRepository;

   Импорт интерфейса CrudRepository из Spring Data, который предоставляет базовые операции CRUD
   (Create, Read, Update, Delete) для работы с сущностями.

     import com.example.NewsPortal.entitys.NewsEntity;: Импорт сущности NewsEntity,
     с которой будет работать этот репозиторий.

    public interface NewsRepo extends CrudRepository<NewsEntity, Long> {: Определение интерфейса NewsRepo,
    который наследует CrudRepository. В этом интерфейсе указывается тип сущности
    (NewsEntity) и тип её уникального идентификатора (Long).

    Интерфейс NewsRepo предоставляет основные методы для выполнения операций CRUD
    (создание, чтение, обновление и удаление) для сущностей NewsEntity в базе данных. Spring
    Data будет автоматически генерировать реализацию этого интерфейса, что позволяет выполнять
    операции с базой данных без написания дополнительного кода.*/
