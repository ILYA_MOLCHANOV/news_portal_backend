
/*Этот код определяет сервисный класс NewsServices,
        который предоставляет методы для работы с сущностями новостей.*/

package com.example.NewsPortal.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.NewsPortal.entitys.NewsEntity;
import com.example.NewsPortal.repository.NewsRepo;

// Объявляем этот класс как сервисный компонент Spring.
@Service
public class NewsServices {

    // Используется для внедрения (инъекции) зависимости NewsRepo в этот сервис.
    @Autowired
    private NewsRepo newsRepo;

    // Метод для создания новой статьи.
    public NewsEntity createArticle(NewsEntity article) {
        // Устанавливаем изображение для статьи.
        article.setImage("https://loremflickr.com/640/480?lock=7837779327188992");
        // Сохраняем статью в базе данных с помощью репозитория NewsRepo.
        return newsRepo.save(article);
    }

    // Метод для получения всех новостей.
    public Iterable<NewsEntity> getAllNews() {
        // Получаем все новости из базы данных с помощью репозитория NewsRepo.
        return newsRepo.findAll();
    }
}
