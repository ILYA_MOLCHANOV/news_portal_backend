package com.example.NewsPortal.entitys;

import java.time.LocalDateTime;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

// Объявляем класс как сущность, чтобы он мог быть сохранен в базе данных.
@Entity
public class NewsEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    // Уникальный идентификатор статьи.
    private Long id;
    // Заголовок статьи.
    private String title;

    @Column(columnDefinition = "TEXT")
    // Содержание статьи
    private String content;
    // Путь к изображению, связанному со статьей.
    private String image;
    @Column(updatable = false)
    @CreationTimestamp
    // Дата и время создания статьи, автоматически заполняется при создании.
    private LocalDateTime createdAt;
    @UpdateTimestamp

    // Дата и время последнего обновления статьи, автоматически обновляется при изменении.
    private LocalDateTime updatedAt;

    // Конструктор без параметров.
    public NewsEntity(){}

    // Геттеры и сеттеры для всех полей.
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) { 
        this.title = title;
    }
    public String getContent() {
        return content;
    }
    public void setContent(String content) {
        this.content = content;
    }
    public String getImage() {
        return image;
    }
    public void setImage(String image) {
        this.image = image;
    }
    public LocalDateTime getCreatedAt() {
        return createdAt;
    }
}

   /* Этот класс NewsEntity описывает сущность новостной статьи, которая будет сохранена в базе данных.
        Он использует аннотации JPA (Java Persistence API) для маппинга на структуру базы данных,
        такие как @Entity, @Id, @Column, @GeneratedValue, @CreationTimestamp, и @UpdateTimestamp.
        Каждое поле соответствует столбцу в таблице базы данных, и они имеют геттеры и сеттеры
        для доступа к данным.*/
