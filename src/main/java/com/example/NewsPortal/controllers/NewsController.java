package com.example.NewsPortal.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.NewsPortal.entitys.NewsEntity;
import com.example.NewsPortal.services.NewsServices;

// Этот класс является контроллером Spring MVC и обрабатывает HTTP-запросы.
@RestController

// Указывает базовый URL-путь для всех методов в этом контроллере.
@RequestMapping("/news")
public class NewsController {

    // Используется для внедрения (инъекции) зависимости NewsServices в этот контроллер.
    @Autowired
    private NewsServices newsServices;

    // Обработчик HTTP POST-запросов на путь /news.
    @PostMapping()
    public ResponseEntity createArticle(@RequestBody NewsEntity article) {
        try {
            // Вызывает метод создания статьи из NewsServices.
            newsServices.createArticle(article);
            // Возвращает успешный HTTP-ответ.
            return ResponseEntity.ok("Пост успешно создан");
        } catch (Exception e) {
            // Возвращает HTTP-ответ с ошибкой в случае исключения.
            return ResponseEntity.badRequest().body("Произошла ошибка");
        }
    }

    // Обработчик HTTP GET-запросов на путь /news.
    @GetMapping()
    public ResponseEntity getAllNews() {
        try {
            // Возвращает список новостей в успешном HTTP-ответе.
            return ResponseEntity.ok(newsServices.getAllNews());
        } catch (Exception e) {
            // Возвращает HTTP-ответ с ошибкой в случае исключения.
            return ResponseEntity.badRequest().body("Произошла ошибка");
        }
    }
}

/*
@RestController: Эта аннотация помечает класс как контроллер Spring MVC, что означает, что он обрабатывает
        HTTP-запросы.

@RequestMapping("/news"): Устанавливает базовый URL-путь для всех методов в этом контроллере.
        Все методы будут доступны по адресу /news.

@Autowired: Эта аннотация указывает Spring на внедрение (инъекцию) зависимости NewsServices в этот контроллер.

@PostMapping(): Обработчик HTTP POST-запросов на путь /news. Метод createArticle
        принимает JSON-объект NewsEntity в качестве параметра.

@GetMapping(): Обработчик HTTP GET-запросов на путь /news. Метод getAllNews возвращает список
        новостей в виде HTTP-ответа. В обоих методах используется блок try-catch для обработки возможных исключений.
        Если операции успешны, возвращается успешный HTTP-ответ с кодом 200 (OK),
        иначе возвращается HTTP-ответ с кодом 400 (Bad Request) и сообщением об ошибке.*/
