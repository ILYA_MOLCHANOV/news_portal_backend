package com.example.NewsPortal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication // Эта аннотация указывает, что это главный класс Spring Boot приложения.
public class NewsPortalApplication {

	// Этот метод запускает Spring Boot приложение.
	public static void main(String[] args) {
		SpringApplication.run(NewsPortalApplication.class, args);
	}

}
